# Copyright (c) 2015, Ben Lopatin
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.  Redistributions in binary
# form must reproduce the above copyright notice, this list of conditions and the
# following disclaimer in the documentation and/or other materials provided with
# the distribution
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Classes for structuring the package environmentment and building dependency
relationships.

Use a graph where the nodes are pkg_resources.Distribution instances and the
edges are pkg_resources.Requirement instances.
"""

import collections
import networkx as nx


class Environment(nx.DiGraph):
    """
    Directed graph representing the installed packages and their dependency
    relationships.
    """

    @classmethod
    def create(cls, working_set):
        """
        Creates a new Environment instance and then builds the graph from the
        provided installed package iterator.

        :param working_set: an iterator of pkg_resources.Dependency instances
        :returns: an Environment instance
        """
        g = Environment()
        return g.build_environment(working_set)

    def build_environment(self, working_set):
        """
        Builds a graph of the environmentment

        :param working_set: an iterator of pkg_resources.Dependency instances
        :returns: a networx.DiGraph instance
        """
        self.add_nodes_from(working_set)

        # Want to have these in same order as the graph
        self.packages = collections.OrderedDict(((pkg.key, pkg) for pkg in self.nodes()))
        for node in self.nodes():
            for requirement in node.requires():
                self.add_edge(node, self.packages[requirement.key],
                        specs=requirement.specs or [('>=', '0.0.0')])

        return self

    def package_requires(self, package_name):
        """
        Returns a list of the immediate packages that package_name requires

        :param package_name: a string for the package key
        """
        return self.successors(self.packages[package_name])

    def standalone_nodes(self):
        """
        Return a set of nodes that have no connections

        :returns: a set of pkg_resources.Dependency instances
        """
        return set((n for n in self.nodes() if not self.successors(n) and not
            self.predecessors(n)))

    def top_level_nodes(self):
        """
        Return a set of nodes that have no packages requiring them.

        :returns: a set of pkg_resources.Dependency instances
        """
        return set((n for n in self.nodes() if not self.predecessors(n)))

    def package_requires_full(self, package_name, sub=None, installed=None):
        """
        Returns a list of packages that package_name requires, including
        sub-dependencies.

        :param package_name: a string for the package key
        :param sub: a subgraph of the parent graph featuring only nodes
            required by the specified package
        :param installed: a set of package keys used for tracking which have
            been added to the graph already.
        :returns: a subgraph of the `environment` graph
        """
        if sub is None:
            sub = nx.DiGraph()

        # In the event of a cyclic dependency the program will throw a
        # RuntimeError by exceeding the maximum recursion depth. This ensures
        # that does not happen.
        if installed is None:
            installed = set()

        for dependency in self.package_requires(package_name):
            if dependency.key in installed:
                continue
            installed.add(dependency.key)
            sub.add_node(dependency)
            sub = self.package_requires_full(dependency.key, sub, installed)
        return sub

    def packages_requiring(self, package_name):
        """
        Returns a list of the immediate packages that require package_name

        :param package_name: a string for the package key
        """
        return self.predecessors(self.packages[package_name])

    def packages_requiring_full(self, package_name, sub=None, installed=None):
        """
        Returns a list of packages that require package_name, including
        upstream parents.

        :param package_name: a string for the package key
        :param sub: a subgraph of the parent graph featuring only nodes
            requiring the specified package
        :param installed: a set of package keys used for tracking which have
            been added to the graph already.
        :returns: a subgraph of the `environment` graph
        """
        if sub is None:
            sub = nx.DiGraph()

        # In the event of a cyclic dependency the program will generate a
        # RuntimeError by exceeding the maximum recursion depth. This ensures
        # that does not happen.
        if installed is None:
            installed = set()

        for dependency in self.packages_requiring(package_name):
            if dependency.key in installed:
                continue
            installed.add(dependency.key)
            sub.add_node(dependency)
            sub = self.packages_requiring_full(dependency.key, sub, installed)
        return sub

    def print_graph(self, file_object):
        """
        """
        try:
            nx.drawing.nx_agraph.write_dot(self, file_object)
        except ImportError:
            # User does not have pygraphviz installed
            raise

