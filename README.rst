========
Magwitch
========

A little tool built on top of `pip` for understanding Python package
dependencies.

.. image:: docs/pip-magwitch.jpg

While I have a great expectations for what this project will do, it is pretty
basic to start with - by design - and very much in flux.

Project goals
=============

- Show auxilliary information about a package, including not just the
  packages it requires, but its entire dependency lineage
- Show the same lineage but for packages which require a given package
- Show version specifications in a lineage, including conflicts and how wide the
  version window is (e.g. if package A requires package X between versions 1.0
  and 2.0, and package B requires package X between versions 0.9 and 1.5, then
  the specification window is 1.0 through 1.5)
- Generate a dependency graphs mapping version specification relationsips
- Generate Graphiz and image represenations of the dependency graph
- Provide CLI interface as well as reusable modules

Python 3 gets first class support with backwards support for Python 2.7 if it
doesn't get in the way.

Installation
============

Download source and run::

    python setup.py install

Usage
=====

Magwitch uses the script name `abel`.

**Note that functionality documented here describes functionality as planned,
not necessarily as it works.**

requires
--------

See what other packages a given package depends on.::

    $ abel requires sphinx
    snowballstemmber
    six
    alabaster
    docutils
    jinja2
    pygments
    sphinx-rtd-theme
    babel

Options
~~~~~~~

    `--full`
        Produce a list recursively of packages required by the original
        package.

    `--tree`
        An output flat for generating a `full` list as a tree of requirements.

required_by
-----------

See what other packages require the given package.::

    $ abel required_by sphinx
    sphinx-rtd-theme

Options
~~~~~~~

    `--full`
        Produce a list recursively of packages dependent on the original
        package.

    `--tree`
        An output flat for generating a `full` list as a tree of requirements.


orphans
-------

See what packages are installed without any specification.::

    $ abel orphans
    Sphinx

Note that without specifying requirement files this will produce false
positives.::

    $ abel orpans -f requirements.txt -f requirements-dev.txt

freeze
------

Like `pip freeze` but separates out specified packages from dependent packages
and documents the source of the requirement.::

    $ abel freeze -f requirements.txt
    # Package dependencies
    requests==2.0             # SomePackage (>=1.0), another_one (>4.0,<4.5)
    gunicorn==19.1.0          # WebApp (==19.1.0)

    # Specified dependencies
    WebApp==1.3

Data model
==========

Magwitch uses a graph modeling each package as a node, and each dependency link
as an edge. This includes both package dependencies and requirement file
dependencies.

TODO
====

- Implement requirement file handling
- Exclude magwitch and its own dependencies from reporting (make this default
  but should be able toggle)
- Implement a `freeze` command

