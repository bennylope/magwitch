#!/usr/bin/env python
# -*- coding: utf-8 -*-

import magwitch
from setuptools import setup

version = magwitch.__version__

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='magwitch',
    version=version,
    description="""Package info.""",
    long_description=readme + '\n\n' + history,
    author='Ben Lopatin',
    author_email='ben@benlopatin.com',
    url='https://github.com/bennylope/magwitch',
    packages=[
        'magwitch',
    ],
    include_package_data=True,
    install_requires=[
        'pip',
        'click',
        'networkx',
    ],
    license="BSD",
    zip_safe=False,
    keywords='magwitch',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
    ],
    entry_points={
        'console_scripts': [
            'abel = magwitch:main',
        ],
    }
)
